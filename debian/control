Source: dafny
Section: devel
Priority: optional
Maintainer: Benjamin Barenblat <bbaren@mit.edu>
Build-Depends: debhelper-compat (= 12),
               cli-common-dev,
               libboogie-cil,
               mono-devel
Standards-Version: 4.4.1
Homepage: https://research.microsoft.com/en-us/projects/dafny/
Vcs-Git: https://salsa.debian.org/debian/dafny.git
Vcs-Browser: https://salsa.debian.org/debian/dafny

Package: dafny
Architecture: all
Depends:
 mono-mcs,
 z3,
 ${cli:Depends},
 ${misc:Depends}
Description: programming language with program correctness verifier
 Dafny is a programming language with a program verifier.  The verifier
 processes function preconditions, postconditions, and assertions, and sends
 them to an SMT solver for checking.  In this way, assertion failures become
 compiler errors, rather than runtime ones.
